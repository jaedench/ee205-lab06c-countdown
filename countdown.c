///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts up (or from) a significant date. The current date is set to January 1st, 1970.
//
// Example:
//   $./countdown
//   Reference time: Thu Jan 01 00:00:00 AM HST 1970
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 13 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 14 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 15 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 16 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 17 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 18 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 19 
//   Years 52 Days: 64 Hours: 22 Minutes: 14 Seconds: 20 
//
// @author Jaeden Chang <jaedench@hawaii.edu>
// @date   20_feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h> 
#include <stdbool.h>
#include <string.h>

const int REFERENCE_YEAR   = 1970;
const int REFERENCE_MONTH  = 01;
const int REFERENCE_DAY    = 01;
const int REFERENCE_HOUR   = 0;
const int REFERENCE_MIN    = 0;

const int secPerMin  = 60;
const int secPerHr   = 3600;
const int secPerDay  = 86400;
const int secPerYr   = 31536000;


void printDifference(double secs) {
   long differenceInYears = (long) secs / secPerYr;
        secs = secs - (differenceInYears * secPerYr);

   long differenceInDays = (long) secs / secPerDay;
        secs = secs - (differenceInDays * secPerDay);

   long differenceInHours = (long) secs / secPerHr;
        secs = secs - (differenceInHours * secPerHr);

   long differenceInMinutes = (long) secs / secPerMin;
        secs = secs - (differenceInMinutes * secPerMin);
   
   long differenceInSeconds = secs;


   printf("Years %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %ld \n"
         , differenceInYears
         , differenceInDays
         , differenceInHours
         , differenceInMinutes
         , differenceInSeconds);
}


int main() {
   time_t referenceTime = -1;

   struct tm reference;

   memset(&reference, 0, sizeof(reference));

   reference.tm_year = REFERENCE_YEAR - 1900;
   reference.tm_mon  = REFERENCE_MONTH - 1;
   reference.tm_mday = REFERENCE_DAY;
   reference.tm_hour = REFERENCE_HOUR;
   reference.tm_min  = REFERENCE_MIN;
   
   // convert the tm reference structure into epoch time
   referenceTime = mktime(&reference);

   if( referenceTime == -1) {
      printf("Cannot make a reference time.\n");
      exit(EXIT_FAILURE);
   }
   
   char buf[80];

   // get current time
   time_t now = time(&now);

   reference = *localtime(&referenceTime);
   strftime(buf, sizeof(buf), "%a %b %d %X %p %Z %Y\n", &reference);
   printf("Reference time: %s", buf);
   
   //finds difference between ref time and now
   double epochTime = difftime(now, referenceTime);

   while(true) {
      printDifference(epochTime);
      epochTime = epochTime + 1;
      
      sleep(1);
   }

   return EXIT_SUCCESS;
}
